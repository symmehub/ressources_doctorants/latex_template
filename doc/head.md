# Latex template

## Description 
This repository is an example of how to combine :
- Templates for different latex documents
- Gitlab's continuous integration tool
- Document sharing using the Gitlab "Page" tool

The CI/CD pipeline consists of two steps :

  1. **building-latex**: Compile yours .tex files and store its into artifacts.
  2. **pages**: From artifacts publish .pdf outputs of your .tex files.

## Requirements

A Python installation is mandatory for this project. Python must be available in your environment variable.

You can see the following tutorial [here](https://docs.anaconda.com/anaconda/install/index.html) to install Python thought Anaconda.

**Don't forget to add Python to your path environment ([reference](https://docs.anaconda.com/anaconda/user-guide/faq/))**

## How to use ?
**Note: Steps 1 and 2 are only necessary for the first time you use this repository.**
**The other steps should be repeated depending on your use.**

1. **Setup** git hooks ***pre/post-commit***:

    In order to update the links of your README.md file, you have to configure pre/post-commit hooks.
    
   - What do you mean ?
    
      Actually, you need at every commit to update your README.md file with the URL links of yours writing works. 
      
      So when you commit your work, the **update_README.py** python script is automatically run using the pre-commit hook. Then, the output of this script which is the update of your README.md with your links is added to your commit using the post-commit hook.

    The configuration of requested hooks is described in the following:

   - Mac\Linux's user:

        - Open a terminal at the root of project, create a symlink as describe :  

                cd ./.git/hooks
                ln -s -f ../../doc/hooks/pre-commit pre-commit
                ln -s -f ../../doc/hooks/post-commit post-commit

        
            If you enter **ls -la** command you should have the following outcome:

            ![alt text](https://gitlab.com/symmehub/ressources_doctorants/latex_template/-/raw/main/doc/figures/screenshot_symlink_check.png)

        - Set excution rights to file in *project_root_name/doc/hooks/* for all files:

                chmod +x ./doc/hooks/*

    - Windows's user (not tested !):
    
        Open a terminal at the root of project, create a symlink as describe :  

            cd ./.git/hooks
            mklink ../../doc/hooks/pre-commit pre-commit
            mklink ../../doc/hooks/post-commit post-commit



3. **Get** your Gitlab pages URL name and update python script called **update_README.py**.
   - Go to your repository on GitLab website.
   - Go to *Settings>Pages* and spot your pages URL name. For furthermore information see directly gitlab documentation [here](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html).
   
        Example
        <p align="center">
        <!-- <img src="./figures/gitlab_page_name.png" alt="drawing" width="300"/> -->
        <img src=https://gitlab.com/symmehub/ressources_doctorants/latex_template/-/raw/main/doc/figures/gitlab_page_name.png alt="drawing" width="300"/>
        </p>


    Here the URL name is *https://symmehub.gitlab.io/ressources_doctorants/latex_template*

    - Open **update_README.py** python script, update variable called *gitlabPage_name* at line 6 with your own URL and finally save your changes.

    Example

    ![alt text](https://gitlab.com/symmehub/ressources_doctorants/latex_template/-/raw/4f6690e95ace5e0b27b00c1deeeb0405a960d67f/doc/figures/python_gitlab_page.png)





4. **Create** *.tex files at *"./presentations"* or *"./reports"* or *".thesis/fichiers_latex/Manuscrit"*

    Example

        presentations/PRES_20YY_MM_DD.tex

5. **[NOT REQUIRED]** if you have followed step **1.**
  
    **Run python** script *"update_README.py"* at the root of the repository.
   
   The README.md file will be updated with the name and link of your *.tex files.  

   Example

       - [PRES_20YY_MM_DD.pdf](https://symmehub.gitlab.io/ressources_doctorants/latex_template//PRES_20YY_MM_DD.pdf)


6. **Stage** and **commit** your work. **Important**, if you want to trigger the CI/CD pipeline, you must add ***[ci-run]*** to your commit message.

    Example 

        git add presentations/PRES_20YY_MM_DD.tex
        git add README.md
        git commit -m "[ci-run] Add presentation"

7. You can also directly trigger the CI/CD pipeline from section *Pipeline* on your gitlab repository page and hit button *Run Pipeline* as described below.

    Example
    <p align="center">
    <!-- <img src="./figures/gitlab_page_name.png" alt="drawing" width="300"/> -->
    <img src=https://gitlab.com/symmehub/ressources_doctorants/latex_template/-/raw/main/doc/figures/Trigger_web.png alt="drawing" width="650"/>
    </p>

        

7. Go to your repository on Gitlab, (example [here](https://gitlab.com/symmehub/ressources_doctorants/latex_template.git)) and see created links to yours writing works

